# Contributing

## Clone and compile

Clone the [QMK Firmware](https://github.com/qmk/qmk_firmware) and follow the [environment setup](https://docs.qmk.fm/#/newbs_getting_started?id=environment-setup).

Then clone this project into the `keyboards/ergodox_ez/keymaps` folder:

```bash
cd keyboards/ergodox_ez/keymaps
git clone https://gitlab.com/brezel/public/bepo/ergodox-bepo.git

```

If environment is correctly configured, you can build the firmware with:

```bash
make ergodox_ez:ergodox-bepo

```
